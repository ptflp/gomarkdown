package gomarkdown

import (
	"fmt"
	"io"
	"os"
	"regexp"
	"strings"
)

// TODO: change to iota
const (
	Header    = "header"
	Paragraph = "paragraph"
	Quote     = "quote"
	//CodeBlock = "codeblock"
	//ListItem  = "listitem"
)

type AdditionalFielder interface {
	GetFields() map[string]interface{}
}

type MarkDown struct {
	DocumentMD *Container
}

type Container struct {
	Parent           *Container `yaml:"-"`
	Type             string
	Content          string
	Level            int
	Childs           []*Container
	AdditionalFields AdditionalFielder
}

func (m *MarkDown) ParseFile(name string) *MarkDown {
	content, err := os.ReadFile(name)
	if err != nil {
		return m
	}

	return m.Parse(content)
}

func (m *MarkDown) Parse(markdownRaw []byte) *MarkDown {
	// TODO: add more block types
	markdownText := string(markdownRaw)
	lines := strings.Split(markdownText, "\n")
	currentNode := &Container{}
	m.DocumentMD = currentNode

	for _, line := range lines {
		line = strings.TrimSpace(line)

		// Проверяем заголовки
		switch {
		case getBool(line, isHeader):
			_, level := isHeader(line)
			line = strings.TrimSpace(strings.ReplaceAll(line, "#", ""))
			for currentNode != nil && level <= currentNode.Level {
				currentNode = SearchParent(level-1, currentNode)
			}
			newNode := &Container{
				Parent:  currentNode,
				Level:   level,
				Type:    Header,
				Content: line,
			}
			currentNode.Childs = append(currentNode.Childs, newNode)
			currentNode = newNode

		case getBool(line, isQuote):
			line = strings.TrimSpace(strings.ReplaceAll(line, ">", ""))
			newNode := &Container{
				Parent:  currentNode,
				Type:    Quote,
				Content: line,
			}
			currentNode.Childs = append(currentNode.Childs, newNode)
		default:
			if line == "" {
				continue
			}
			newNode := &Container{
				Parent:  currentNode,
				Type:    Paragraph,
				Content: line,
			}
			currentNode.Childs = append(currentNode.Childs, newNode)
		}
	}

	return m
}

func SearchParent(level int, node *Container) *Container {
	if node.Level == level {
		return node
	}
	if node.Parent == nil {
		return nil
	}
	return SearchParent(level, node.Parent)
}

func (m *MarkDown) RenderMD(w io.Writer, res *[]byte) *MarkDown {
	// TODO: use strings builder
	if m.DocumentMD == nil {
		return nil
	}
	var data []byte
	// dfs implementation m.DocumentMD
	var recurse func(node *Container)
	recurse = func(node *Container) {
		switch node.Type {
		case Header:
			if node.Content == "" {
				break
			}
			data = append(data, []byte(fmt.Sprintf("\n%s %s\n", strings.Repeat("#", node.Level), node.Content))...)
		case Paragraph:
			data = append(data, []byte(fmt.Sprintf("%s\n", node.Content))...)
		case Quote:
			data = append(data, []byte(fmt.Sprintf("> %s\n", node.Content))...)
		}
		for _, child := range node.Childs {
			recurse(child)
		}
	}
	recurse(m.DocumentMD)
	if w != nil {
		_, err := w.Write(data)
		if err != nil {
			fmt.Println(err)
		}
	}

	if res != nil {
		*res = data
	}

	return m
}

func isHeader(line string) (bool, int) {
	if match, _ := regexp.MatchString(`^#+`, line); match {
		level := strings.Count(line, "#")
		return true, level
	}
	return false, 0
}

func isQuote(line string) (bool, int) {
	if match, _ := regexp.MatchString(`^>`, line); match {
		return true, 0
	}
	return false, 0
}

func getBool(line string, f func(line string) (bool, int)) bool {
	b, _ := f(line)

	return b
}

func (m *MarkDown) Search(nodeType, content string, level int, result *[]*Container) *MarkDown {
	preSearched := make([][]*Container, 0, 3)
	if nodeType != "" {
		preSearched = append(preSearched, m.findNodesByType(m.DocumentMD, nodeType))
	}
	if content != "" {
		preSearched = append(preSearched, m.findNodesByContent(m.DocumentMD, content))
	}
	if level != 0 {
		preSearched = append(preSearched, m.findNodesByLevel(m.DocumentMD, level))
	}
	*result = findIntersections(preSearched)

	return m
}

func (m *MarkDown) Insert(parent, child *Container) *MarkDown {
	child.Level = parent.Level + 1
	child.Parent = parent
	parent.Childs = append(parent.Childs, child)

	return m
}

func (m *MarkDown) findNodesByType(node *Container, nodeType string) []*Container {
	var result []*Container

	if node.Type == nodeType {
		result = append(result, node)
	}

	for _, child := range node.Childs {
		result = append(result, m.findNodesByType(child, nodeType)...)
	}

	return result
}

func (m *MarkDown) findNodesByContent(node *Container, content string) []*Container {
	var result []*Container

	if strings.Contains(node.Content, content) {
		result = append(result, node)
	}

	for _, child := range node.Childs {
		result = append(result, m.findNodesByContent(child, content)...)
	}

	return result
}

func (m *MarkDown) findNodesByLevel(node *Container, level int) []*Container {
	var result []*Container

	if node.Level == level {
		result = append(result, node)
	}

	for _, child := range node.Childs {
		result = append(result, m.findNodesByLevel(child, level)...)
	}

	return result
}

func findIntersections(nodes [][]*Container) []*Container {
	// Создаем карту для подсчета количества вхождений узлов
	counts := make(map[*Container]int)

	// Проходим по каждому массиву
	for _, arr := range nodes {
		// Помечаем узлы текущего массива
		visited := make(map[*Container]bool)

		// Подсчитываем количество вхождений узлов
		for _, node := range arr {
			// Если узел уже был помечен в предыдущих массивах, увеличиваем счетчик
			if visited[node] {
				counts[node]++
			} else {
				// Иначе помечаем узел и устанавливаем счетчик в 1
				visited[node] = true
				counts[node] = 1
			}
		}
	}

	// Ищем узлы с количеством вхождений равным количеству массивов
	var result []*Container
	for node, count := range counts {
		if count == len(nodes) {
			result = append(result, node)
		}
	}

	return result
}
