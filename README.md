# Go markdown parser and renderer

Just for internal projects, to simplify markdown parsing and rendering.

This package is not optimized for public projects, because it uses regexp for parsing, no string.Builder and etc.
> use blackfriday or goldmark for public projects

## Usage

```go
	// Чтение Markdown файла
	var md MarkDown
	f, err := os.OpenFile("default.md", os.O_RDWR|os.O_TRUNC, 0755) // output file
	if err != nil {
		panic(err)
	}
	defer f.Close()
	md.ParseFile("default.mmd") // parsing markdown file, input file

	var searchResult []*Node
	var data []byte
	md.Search("", "создание файла", 0, &searchResult). // searching nodes
		Insert(searchResult[0], &Node{ // inserting nodes
		Parent:  nil,
		Childs:  nil,
		Type:    Header,
		Content: "testiruem indent",
	}).RenderMD(f, &data) // render to file and to bytes slice

	fmt.Println(string(data))
```